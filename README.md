# Running Shiny via Docker

This is a minimal example demonstrating how to serve
an R Shiny application via a Docker image.

## Prerequisites

### Install Docker

I ended up just installing the .deb files for my distribution, 
e.g., from here,
https://download.docker.com/linux/ubuntu/dists/hirsute/pool/stable/amd64/, with the usual `sudo dpkg -i ~/name/of/pkg.deb`.

## Create app

I think it makes sense to separate all the application code
(including data, etc.) into an `app` folder so that we can
specify to only copy that folder into the Docker image
(and therefore ignore this README, the Git repo, and any
other incidental files/folders in the root directory).

So I just made a file `app/app.R` with a minimal Shiny app.

## Create the Dockerfile

We use a Dockerfile to specify how the image is to be built.
All I'm doing is pulling down the [rocker/shiny-verse](https://hub.docker.com/r/rocker/shiny-verse) image, copying the application code (in app/app.R),
and running the `shiny-server` service, i.e., the service
that serves the app.

It seems there are at least two options for running Shiny apps
in Docker containers:
1. Run using `shiny::runApp`, i.e., use the development web server; or
2. Run using Shiny Server Open Source itself to serve the app
(via `/usr/bin/shiny-server`).

I think the second option is probably the best, given you
will (probably, untested) be able to access logs, and do 
other things that the Shiny Server Open Source offers that
the local dev server doesn't.

For completeness, here's an example of a Dockerfile that
operates using the local dev server:

```
FROM rocker/shiny-verse:latest
COPY ./app/* .
CMD R -e "shiny::runApp('app.R', port = 3838, host = '0.0.0.0')"
```

This assumes that your `app.R` file is within a folder `app/` 
in the root directory of the project (i.e., same directory
as the Dockerfile, and as shown in this repo).

I think the second method is better; the Dockerfile for that
is shown in this repo, but for completeness:

```
FROM rocker/shiny-verse:latest
COPY ./app/* /srv/shiny-server/
CMD ["/usr/bin/shiny-server"]
```

Note that in this second configuration, we don't actually
specify which port to expose. Shiny Server Open Source exposes
3838 by default.

Note that this Dockerfile code is essentially copied from stevec from [this Stack Overflow post](https://stackoverflow.com/questions/57421577/how-to-run-r-shiny-app-in-docker-container).

## Build the image

I'm just naming the image "hello-world-shiny". (We next
use that tag/name to run the container.)
The period says to build the image
based on the Dockerfile in the current directory. It'll take a while
to build the first time
because it needs to download rocker/shiny-verse which has R, tidyverse
and shiny installed, etc.

```
sudo docker build -t hello-world-shiny .
```

## Start the app container

`-p` is the port flag; it maps the container port (3838, which
is what `shiny-server` exposes by default)
to the host port (3000 in this case -- just chose that randomly).

```
sudo docker run -dp 3000:3838 hello-world-shiny
```

## Check that it works

Then just go to localhost:3000 and there you have it.

## References

- https://docs.docker.com/get-started/02_our_app/

- https://stackoverflow.com/questions/57421577/how-to-run-r-shiny-app-in-docker-container

- https://www.r-bloggers.com/2021/06/running-shiny-server-in-docker/