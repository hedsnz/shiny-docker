FROM rocker/shiny-verse:latest
COPY ./app/* /srv/shiny-server/
CMD ["/usr/bin/shiny-server"]